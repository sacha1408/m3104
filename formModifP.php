<?php
require 'connexionBD.php';
include 'outils.php';
IsConnectedSession();
connectedAndLogout();

$idp = $_POST['idP'];

$reqPat = $linkpdo->prepare("SELECT * from `Patient` where idP = ?");
$res = $reqPat->execute(array($idp));

if($res == false){
  echo 'erreur lors de la récupération du patient.';
}
$patient = $reqPat -> fetch();

?>
<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <title>Léa passion PACES</title>
    </head>
    <body>
      <script src="bootstrap/js/jquery.js"></script>
      <script src="bootstrap/js/bootstrap.min.js"></script>

	    <form action="modP.php" method="post">
        <input type='hidden' name='idp' value='<?php echo $idp;?>'>
	       <p style="margin-left:10px">Civilité : <input type="text" style="margin-left:5px" name="civ" value='<?php echo $patient[1];?>'/></p>
	       <p style="margin-left:10px">Nom : <input type="text" style="margin-left:5px" name="nom" value='<?php echo $patient[2];?>'/></p>
	       <p style="margin-left:10px">Prénoms : <input type="text" style="margin-left:5px" name="prenoms" value='<?php echo $patient[3];?>'/></p>
         <p style="margin-left:10px">Adresse :<input type="text" style="margin-left:5px" name="addr" value='<?php echo $patient[4];?>'/></p>
	       <p style="margin-left:10px">Code Postal :<input type="text" style="margin-left:5px" name="cp" value='<?php echo $patient[5];?>'/></p>
	       <p style="margin-left:10px">Ville :<input type="text" style="margin-left:5px" name="ville" value='<?php echo $patient[6];?>'/></p>
	       <p style="margin-left:10px">Date de naissance :<input type="date" style="margin-left:5px" name="dateN" value='<?php echo $patient[7];?>'/></p>
         <p style="margin-left:10px">Lieu de naissance :<input type="text" style="margin-left:5px" name="lieuN" value='<?php echo $patient[8];?>'/></p>
         <p style="margin-left:10px">Médecin référent : <select name="medRef"><option value='0'>Aucun</option>
         <?php
          $req = $linkpdo->query('SELECT * FROM `Medecin`');

          while($row = $req->fetch()){
            if($row[0] == $patient[10]){
              echo "<option name='medRef' value='$row[0]' selected>".$row[1]." ".$row[3]." ".$row[2]."</option>";
            }else{
              echo "<option name='medRef' value='$row[0]'>".$row[1]." ".$row[3]." ".$row[2]."</option>";
            }
          }?>
        </select><br><br>
	      <input type="submit" class="btn btn-outline-success" name="Modifier contact"/><input type="reset" class="btn btn-outline-danger" name="Effacer" style="margin-left:5px"/>
	    </form>
    </body>
</html>
