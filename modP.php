<?php

  require 'connexionBD.php';
  include 'outils.php';
  IsConnectedSession();
  connectedAndLogout();

	$idp=substr($_POST['idp'], 0, -1);
	$civ=$_POST['civ'];
	$name=$_POST['nom'];
	$surname=$_POST['prenoms'];
	$addr=$_POST['addr'];
	$cp=$_POST['cp'];
	$city=$_POST['ville'];
	$date=$_POST['dateN'];
	$lieu=$_POST['lieuN'];
  $med=$_POST['medRef'];

	echo "Modification du patient... \n";

  $fin=$linkpdo->prepare("UPDATE `Patient` SET civ=?, nom=?, prenoms=?, addr=?, cp=?, ville=?, dateN=?, lieuN=?, medRef=? where idP=?");
  $resultfin=$fin->execute(array($civ, $name, $surname, $addr, $cp, $city, $date, $lieu, $med, $idp));
  if ($resultfin==FALSE){
    die("Erreur dans la réalisation de la requête d'insertion");
  }else{
    echo "<br>Patient modifié.";
  }
?>
<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <title>Léa passion PACES</title>
    </head>
	<body>
    <script src="bootstrap/js/jquery.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>

	</body>
</html>
