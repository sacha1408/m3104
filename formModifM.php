<?php require 'connexionBD.php';
include 'outils.php';
IsConnectedSession();
connectedAndLogout();

$idM = $_POST['idM'];

$req = $linkpdo->prepare("SELECT * from `Medecin` where idM = ?");
$res = $req->execute(array($idM));

if($res == false){
  echo 'erreur lors de la récupération du patient.';
}
$med = $req -> fetch();


?>
<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <title>Léa passion PACES</title>
    </head>
    <body>
      <script src="bootstrap/js/jquery.js"></script>
      <script src="bootstrap/js/bootstrap.min.js"></script>

	     <form action="putApi.php" method="POST">
         <input type='hidden' name='idM' value='<?php echo $idM;?>'>
	       <p style="margin-left:10px">Civilité : <input type="text" name="civ" value='<?php echo $med[1];?>'/></p>
	       <p style="margin-left:10px">Nom : <input type="text" name="name" value='<?php echo $med[2];?>'/></p>
	       <p style="margin-left:10px">Prénoms : <input type="text" name="surname" value='<?php echo $med[3];?>'/></p>
	       <input type="submit" class="btn btn-outline-success" name="Modifier" style="margin-left:10px"/><input type="reset" class="btn btn-outline-danger" name="Effacer" style="margin-left:5px"/>
	     </form>
     </body>
</html>
