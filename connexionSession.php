<!DOCTYPE HTML>
<?php
  $login = "root";
  $pwd = "iutinfo";

  if (isset($_POST['login']) && isset($_POST['pwd'])) {
    if ($login == $_POST['login'] && $pwd == $_POST['pwd']) {
      session_start();

      $_SESSION['id'] = $_POST['login'];
      $_SESSION['pwd'] = $_POST['pwd'];

      header('Location: index.php');
    }
  }

?>

<html>
    <head>
        <meta charset="utf-8" />
        <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <style>body{background:#f1f7fc;padding:80px 0}form{max-width:320px;width:90%;margin:0 auto;background-color:#fff;padding:40px;border-radius:4px;color:blue;box-shadow:1px 1px 5px rgba(0,0,0,.1)}.illustration{text-align:center;padding:0 0 20px;font-size:100px;color:blue}form .form-control{background:#f7f9fc;border:none;border-bottom:1px solid #dfe7f1;border-radius:0;box-shadow:none;outline:0;color:inherit;text-indent:8px;height:42px}form .btn-primary{background:blue;border:none;border-radius:4px;padding:11px;box-shadow:none;margin-top:26px;text-shadow:none;outline:0!important}form .btn-primary:active,form .btn-primary:hover{background:#eb3b60}form .btn-primary:active{transform:translateY(1px)}form .forgot{display:block;text-align:center;font-size:12px;color:#6f7a85;opacity:.9;text-decoration:none}form .forgot:active,form .forgot:hover{opacity:1;text-decoration:none}</style>
        <title>Léa passion PACES</title>
    </head>
    <body>

      <div class="login-clean">
        <form method="post" action="connexionSession.php">
            <h2 class="sr-only">Login Form</h2>
            <div class="illustration">
                <svg style="width:  50%;" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                    <path fill="currentColor" d="M400 192h-32v-46.6C368 65.8 304 .2 224.4 0 144.8-.2 80 64.5 80 144v48H48c-26.5 0-48 21.5-48 48v224c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48V240c0-26.5-21.5-48-48-48zm-272-48c0-52.9 43.1-96 96-96s96 43.1 96 96v48H128v-48zm272 320H48V240h352v224z"/>
                </svg>
            </div>
            <div class="form-group"><input class="form-control" type="text" name="login" placeholder="Login"></div>
            <div class="form-group"><input class="form-control" type="password" name="pwd" placeholder="Password"></div>
            <div class="form-group">
                <button class="btn btn-primary btn-block" type="submit">Log In</button>
            </div>
        </form>
    </div>

      <script src="bootstrap/js/jquery.js"></script>
      <script src="bootstrap/js/bootstrap.min.js"></script>
    </body>
</html>
