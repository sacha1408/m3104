<?php require 'connexionBD.php';
include 'outils.php';
IsConnectedSession();
connectedAndLogout();?>

<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <title>Léa passion PACES</title>
	      <h1 style="margin-left:10px">Liste des médecins</h1>
    </head>
    <body>
      <script src="bootstrap/js/jquery.js"></script>
      <script src="bootstrap/js/bootstrap.min.js"></script>

       <?php

       	$res = $linkpdo->query('SELECT * FROM `Medecin`');
        //Traitement de la requête
        ?>

        <table class="table table-striped">
          <thead class="thead-dark">
        <tr>
            <th>Civilité</th>
            <th>Nom</th>
            <th>Prénom</th>
        </tr>
      </thead>
        <?php while ($data = $res->fetch()):?>
    <tr><form method='post'>
        <input type='hidden' name='idM' value=<?php echo $data['idM'];?>/>
        <td><?php echo $data['civ']; ?></td>
        <td><?php echo $data['nom']; ?></td>
        <td><?php echo $data['prenoms']; ?></td>
        <td><input type='submit' class='btn btn-outline-primary' name='mod' formaction='formModifM.php' value='Modifier'/><input type='submit'class='btn btn-outline-danger' name='del' formaction='deleteApi.php' value='Supprimer' style='margin-left:5px;'/></td>
        </form>
    </tr>
    <?php endwhile; $res->closeCursor();?>
    </table>

       <form action= "formM.php" method="post">
	        <input type="submit" class="btn btn-outline-primary" name="aj" value="Ajouter" style="padding:5px; margin-left:10px;"/>
       </form>
       <!--boutons grisés qui se débloquent à la sélection d'une ligne>
	        <input type="submit" name="md" value="modifier" style="padding:5px; margin:5px;"/>
          <input type="submit" name="dl" value="supprimer" style="padding:5px; margin:5px;"/>
       <!-->
    </body>
</html>
