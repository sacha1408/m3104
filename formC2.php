<?php require 'connexionBD.php';
include 'outils.php';
IsConnectedSession();
connectedAndLogout();

$pat = $_POST['pat'];

if($pat == 0){
  die("Aucun patient choisi. <br><a href='index.html'> Menu principal </a>");
}?>
<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <title>Léa passion PACES</title>
    </head>
    <body>
      <script src="bootstrap/js/jquery.js"></script>
      <script src="bootstrap/js/bootstrap.min.js"></script>

	    <form action="ajoutC.php" method="post">
        <input type="hidden" name="idP" value=<?php echo $pat;?>>

        <p style="margin-left:10px">Médecin : <select name="med">
          <?php
          //afficher le Medecin referent sil existe
          $req = $linkpdo->prepare('SELECT medRef FROM `Patient` WHERE idP = ?');
          $err = $req->execute(array($pat));

          $idM = $req->fetchColumn();

          if($idM != 0){

            $req = $linkpdo->prepare('SELECT * FROM `Medecin` WHERE idM = ?');
            $err = $req->execute(array($idM));
            $row = $req->fetch();
            echo "<option name='med' value='$idM'>".$row[1]." ".$row[3]." ".$row[2]."</option>";
          }else{
            echo "<option name='med' value=0>Aucun</option>";
            $res = $linkpdo->query('SELECT * FROM `Medecin`');
            while($row = $res->fetch()){

              echo "<option name='med' value='$row[0]'>".$row[1]." ".$row[3]." ".$row[2]."</option>";
            }
          }?>
        </select></p>
        <p style="margin-left:10px">Date de Rendez-vous : <input type="date" name="dateR"/></p>
	      <p style="margin-left:10px">Heure : <input type="time" name="heureD"/></p>
	      <p style="margin-left:10px">Durée : <input type="number" name="temps"/> min</p>

	      <input type="submit" class="btn btn-outline-success" name="Ajouter rendez-vous" style="margin-left:10px"/><input type="reset" class="btn btn-outline-danger" name="Effacer" style="margin-left:10px"/>
	   </form>
   </body>
</html>
