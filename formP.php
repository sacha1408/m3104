<?php require 'connexionBD.php';
include 'outils.php';
IsConnectedSession();
connectedAndLogout(); ?>

<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <title>Léa passion PACES</title>
    </head>
    <body>
      <script src="bootstrap/js/jquery.js"></script>
      <script src="bootstrap/js/bootstrap.min.js"></script>

	    <form action="ajoutP.php" method="post">
	     <p style="margin-left:10px">Civilité :  <select name="civ"><option value='M.'> M.</option>
       <option value='Mme.'> Mme.</option>
       </select></p>
	     <p style="margin-left:10px">Nom : <input type="text" name="nom"/></p>
	     <p style="margin-left:10px">Prénoms : <input type="text" name="prenoms"/></p>
       <p style="margin-left:10px">Adresse :<input type="text" name="addr" style="margin-left:5px"/></p>
	     <p style="margin-left:10px">Code Postal :<input type="text" name="cp" style="margin-left:5px"/></p>
	     <p style="margin-left:10px">Ville :<input type="text" name="ville" style="margin-left:5px"/></p>
	     <p style="margin-left:10px">Date de naissance :<input type="date" name="dateN" style="margin-left:5px"/></p>
       <p style="margin-left:10px">Lieu de naissance :<input type="text" name="lieuN" style="margin-left:5px"/></p>
       <p style="margin-left:10px">Numéro de sécurité sociale :<input type="text" name="numSec" style="margin-left:5px"/></p>
       <p style="margin-left:10px">Médecin référent : <select name="medRef"><option value='0'>Aucun</option>
       <?php

        $res = $linkpdo->query('SELECT * FROM `Medecin`');

        while($row = $res->fetch()){
          echo "<option name='medRef' value='$row[0]'>".$row[1]." ".$row[3]." ".$row[2]."</option>";
        }?>
        </select><br><br>
	     <input type="submit" class="btn btn-outline-success" name="Ajouter contact"/><input type="reset" class="btn btn-outline-danger" name="Effacer" style="margin-left:5px"/>
	    </form>
    </body>
</html>
