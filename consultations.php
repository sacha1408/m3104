<?php require 'connexionBD.php';
include 'outils.php';
IsConnectedSession();
connectedAndLogout();

if(isset($_POST['med'])){
  $med = $_POST['med'];
}else{
  $med=0;
}
?>

<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <title>Léa passion PACES</title>
	      <h1 style='margin-left:10px;'>Liste des consultations</h1>
    </head>
    <body>
      <script src="bootstrap/js/jquery.js"></script>
      <script src="bootstrap/js/bootstrap.min.js"></script>

      <p class="text-sm-left" style='margin-left:10px'>Trier par médecin</p>
      <form method="post" action='consultations.php'>
      <select name="med" style='margin-left:10px; height:35px;'><option name="med" value='0'>Aucun</option>
      <?php

       $res = $linkpdo->query('SELECT * FROM Medecin');

       while($row = $res->fetch()){
         echo "<option name='med' value='$row[0]'>".$row[1]." ".$row[3]." ".$row[2]."</option>";
       }?></select><input type="submit" class="btn btn-outline-success" name="valide"  value="OK" style='margin-left:3px; margin-bottom:5px'/></form>
       <!-- requete SQL select * from patients dans tableau-->
       <?php
        if($med != 0){
          $res = $linkpdo->prepare('SELECT `idP`, DATE_FORMAT(`dateRDV`, "%d/%m/%Y %H:%i") as dRDV, DATE_FORMAT(`dateRDV`, "%Y/%m/%d_%H:%i") as dateRDV, `temps` FROM `rdv` WHERE idM=? ORDER BY `dateRDV`');
          $err = $res->execute(array($med));
        }
        else{
          $res = $linkpdo->prepare('SELECT `idM`, `idP`, DATE_FORMAT(`dateRDV`, "%d/%m/%Y %H:%i") as dRDV, DATE_FORMAT(`dateRDV`, "%Y/%m/%d_%H:%i") as dateRDV, `temps` FROM `rdv` ORDER BY `dateRDV`');
          $err = $res->execute();
        }
        if(!$err){
          die("impossible de récupérer les consultations.");
        }
        //Traitement de la requête
        ?>
       <br>
       <table class="table table-striped">
         <thead class="thead-dark">
          <tr>
            <th>Médecin</th>
            <th>Patient</th>
            <th>Date RDV</th>
            <th>Durée</th>
          </tr>
        </thead>
        <?php while ($data = $res->fetch()):?>
    <tr><form method='post'>
      <!-- affichage du nom du medecin-->
      <td><?php
      if($med !=0){
        $idM = $med;
      }else{
        $idM = $data['idM'];
      }
      if($idM == 0){
        echo "Aucun";
      }else{
        $req2 = $linkpdo->prepare("SELECT * FROM `Medecin` WHERE IdM=?");
        $res2 = $req2->execute(array($idM));
        if($res2){
          $resMed = $req2->fetch();
        }else{
          die("impossible de récupérer le médecin.");
        }
        echo $resMed[1].' '.$resMed[3].' '.$resMed[2];
      }
      ?><input type='hidden' name='idM' value=<?php echo $idM;?>/></td>
      <!-- celui du patient-->
      <td><?php
      $idP = $data['idP'];
      if($idP == 0){
        echo "Aucun";
      }else{
        $req3 = $linkpdo->prepare("SELECT * FROM `Patient` WHERE IdP=?");
        $res3 = $req3->execute(array($idP));
        if($res3){
          $resP = $req3->fetch();
        }else{
          die("impossible de récupérer le patient.");
        }
        echo $resP[1].' '.$resP[3].' '.$resP[2];
      }
      ?>
        <input type='hidden' name='idP' value=<?php echo $idP;?>/></td>
        <td><?php echo $data['dRDV']; ?>
        <input type='hidden' name='dateRDV' value=<?php echo $data['dateRDV'];?>></td>
        <td><?php echo $data['temps']; ?> min</td>
        <td><input type='submit' class='btn btn-outline-danger' name='del' value='Supprimer' formaction='delC.php'/></td>
       </form>
    </tr>
    <?php endwhile;?>
    </table>

       <form action= "formC.php" method="post">
	        <input type="submit" class='btn btn-outline-primary' name="aj" value="Nouvelle consultation" style="padding:5px; margin-left:10px;"/>
       </form>
    </body>
</html>
