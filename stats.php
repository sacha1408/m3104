<?php require 'connexionBD.php';
include 'outils.php';
IsConnectedSession();
connectedAndLogout();
?>
<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <title>Léa passion PACES</title>
	      <h1 style='margin-left:10px'>Statistiques</h1>
    </head>
    <body>
      <script src="bootstrap/js/jquery.js"></script>
      <script src="bootstrap/js/bootstrap.min.js"></script>

      <table class="table table-striped">
        <thead class="thead-dark">
        <tr><th>Tranche d'âge</th>
          <th>Nb Hommes</th>
          <th>Nb Femmes</th>
        </tr>
      </thead>
        <tr>
          <td>Moins de 25 ans</td>
          <td><!--REQUETE HOMMES MOINS DE 25--><?php
            $req = $linkpdo->prepare("SELECT COUNT(*) FROM Patient WHERE civ='M.' AND ROUND(DATEDIFF(NOW(), dateN)/365) < 25");
            $res = $req->execute();
            if($res){
              echo $req->fetchColumn();
            }else{
              die("Erreur lors de l'éxécution de la requête");
            }
          ?></td>
          <td><!--REQUETE FEMMES MOINS DE 25--><?php
            $req = $linkpdo->prepare("SELECT COUNT(*) FROM Patient WHERE civ='Mme.' AND ROUND(DATEDIFF(NOW(), dateN)/365) < 25");
            $res = $req->execute();
            if($res){
              echo $req->fetchColumn();
            }else{
              die("Erreur lors de l'éxécution de la requête");
            }
          ?></td>
        </tr>
        <tr>
          <td>Entre 25 et 50 ans</td>
          <td><!--REQUETE HOMMES 25 50--><?php
            $req = $linkpdo->prepare("SELECT COUNT(*) FROM Patient WHERE civ='M.' AND ROUND(DATEDIFF(NOW(), dateN)/365) > 25 AND ROUND(DATEDIFF(NOW(), dateN)/365) < 50");
            $res = $req->execute();
            if($res){
              echo $req->fetchColumn();
            }else{
              die("Erreur lors de l'éxécution de la requête");
            }
          ?></td>
          <td><!--REQUETE FEMMES 25 50--><?php
            $req = $linkpdo->prepare("SELECT COUNT(*) FROM Patient WHERE civ='Mme.' AND ROUND(DATEDIFF(NOW(), dateN)/365) > 25 AND ROUND(DATEDIFF(NOW(), dateN)/365) < 50");
            $res = $req->execute();
            if($res){
              echo $req->fetchColumn();
            }else{
              die("Erreur lors de l'éxécution de la requête");
            }
          ?></td>
        </tr>
        <tr>
          <td>Plus de 50 ans</td>
          <td><!--REQUETE HOMMES PLUS 50--><?php
            $req = $linkpdo->prepare("SELECT COUNT(*) FROM Patient WHERE civ='M.' AND ROUND(DATEDIFF(NOW(), dateN)/365) > 50");
            $res = $req->execute();
            if($res){
              echo $req->fetchColumn();
            }else{
              die("Erreur lors de l'éxécution de la requête");
            }
          ?></td>
          <td><!--REQUETE FEMMES PLUS 50--><?php
            $req = $linkpdo->prepare("SELECT COUNT(*) FROM Patient WHERE civ='Mme.' AND ROUND(DATEDIFF(NOW(), dateN)/365) > 50");
            $res = $req->execute();
            if($res){
              echo $req->fetchColumn();
            }else{
              die("Erreur lors de l'éxécution de la requête");
            }
          ?></td>
        </tr>
      <thead class="thead-dark">
        <tr><th>Médecin</th>
          <th>Nombre d'heures</th>
        </tr>
      </thead>
        <?php $req = $linkpdo->prepare('SELECT * FROM `Medecin`');
        $res = $req->execute();
        if(!$res){
          die("Erreur lors de la récupération des données");
        }
        while ($row = $req->fetch()):?>
        <tr>
        <?php echo "<td>".$row[1]." ".$row[3]." ".$row[2]."</td>";
          $req2 = $linkpdo->prepare("SELECT ROUND(SUM(temps)/60, 1) AS temps_total FROM rdv r WHERE idM=?");
          $res2 = $req2->execute(array($row[0]));
          if($res2){
            $nb = $req2->fetchColumn();
            if($nb == 0){
              echo "<td> 0 heures</td>";
            }else{
              echo "<td>".$nb." heures</td>";
            }
          }
        ?>
        <?php endwhile;?>
        <tr>
      </table><br>

    </body>
</html>
