<?php
	require 'connexionBD.php';
	include 'outils.php';
	IsConnectedSession();
	connectedAndLogout();

	$idp=$_POST['idP'];

	//verif si pas deja dans BDD
	$req = $linkpdo->prepare("SELECT COUNT(*) FROM `Patient` WHERE  idP= ?");
	$res = $req->execute(array($idp));

	if ($res==FALSE){
		echo "La requête a échoué";
	}else{
		//Traitement de la requête
		$nb = $req->fetchColumn();
		if($nb == 1){
			echo "Suppression du contact... \n";
			$req = $linkpdo->prepare("DELETE FROM `Patient` WHERE idP= ?");
			$res = $req->execute(array($idp));
			if ($res==FALSE){
				echo "La requête a échoué";
			}else{
				echo 'patient supprimé.';
			}
		}else{
			echo 'Erreur : Ce patient n\'existe pas.';
		}
	}
?>
<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="utf-8" />
				<meta http-equiv="X-UA-Compatible" content="IE=edge">
	      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	      <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <title>Léa passion PACES</title>
    </head>
	<body>
		<script src="bootstrap/js/jquery.js"></script>
		<script src="bootstrap/js/bootstrap.min.js"></script>

	</body>
</html>
