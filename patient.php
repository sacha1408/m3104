<?php require 'connexionBD.php';
include 'outils.php';
IsConnectedSession();
connectedAndLogout();?>

<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <title>Léa passion PACES</title>
	      <h1 style='margin-left:10px;'>Liste des patients</h1>
    </head>
    <body>
      <script src="bootstrap/js/jquery.js"></script>
      <script src="bootstrap/js/bootstrap.min.js"></script>

       <!-- requete SQL select * from patients dans tableau-->
       <?php

       	$res = $linkpdo->prepare('SELECT * FROM `Patient`');
        $err = $res->execute();
        if(!$err){
          die("impossible de récupérer les patients.");
        }
        //Traitement de la requête
        ?>
       <br>
       <table class="table table-striped">
         <thead class="thead-dark">
        <tr>
            <th>Civilité</th>
            <th>Nom</th>
            <th>Prénom(s)</th>
            <th>Adresse</th>
            <th>Code Postal</th>
            <th>Ville</th>
            <th>Date de naissance</th>
            <th>Lieu de naissance</th>
            <th>Numéro de sécurité sociale</th>
            <th>Médecin référent</th>
        </tr>
      </thead>
        <?php while ($data = $res->fetch()):?>
    <tr><form method='post'>
        <input type='hidden' name='idP' value=<?php echo $data['idP'];?>/>
        <td><?php echo $data['civ']; ?></td>
        <td><?php echo $data['nom']; ?></td>
        <td><?php echo $data['prenoms']; ?></td>
        <td><?php echo $data['addr']; ?></td>
        <td><?php echo $data['cp']; ?></td>
        <td><?php echo $data['ville']; ?></td>
        <td><?php echo $data['dateN']; ?></td>
        <td><?php echo $data['lieuN']; ?></td>
        <td><?php echo $data['numSec']; ?></td>
        <td><?php
        $idM = $data['medRef'];
        if($idM == 0){
          echo "Aucun";
        }else{
          $req2 = $linkpdo->prepare("SELECT * FROM `Medecin` WHERE IdM=?");
          $res2 = $req2->execute(array($idM));
          if($res2){
            $resMed = $req2->fetch();
          }else{
            die("impossible de récupérer le médecin référent.");
          }
          echo $resMed[1].' '.$resMed[3].' '.$resMed[2];
        }
        ?><input type='hidden' name='medRef' value=<?php echo $idM;?>/></td>
        <td><input type='submit' class='btn btn-outline-primary' name='mod' formaction='formModifP.php' value='Modifier'/><input type='submit' class='btn btn-outline-danger' name='del' formaction='delP.php' value='Supprimer' style='margin-top: 4px;'/></td>
       </form>
    </tr>
    <?php endwhile;?>
    </table>

       <form action= "formP.php" method="post">
	        <input type="submit" class='btn btn-outline-primary' name="aj" value="Ajouter Patient" style="margin-left:10px;"/>
       </form>
       <!--boutons grisés qui se débloquent à la sélection d'une ligne>
	        <input type="submit" name="md" value="modifier" style="padding:5px; margin:5px;"/>
          <input type="submit" name="dl" value="supprimer" style="padding:5px; margin:5px;"/>
       <!-->
    </body>
</html>
