<?php
function IsConnectedSession(){
  session_start();
  if(!isset($_SESSION['id']) && !isset($_SESSION['pwd'])){
    header('Location: connexionSession.php');
  }
}

function connectedAndLogout(){
  echo "<form method='post'>

  <nav class='navbar navbar-expand-lg navbar-light bg-light'>
    <button class='navbar-toggler' type='button' data-toggle='collapse' data-target='#navbarNav' aria-controls='navbarNav' aria-expanded='false' aria-label='Toggle navigation'>
      <span class='navbar-toggler-icon'></span>
    </button>
    <div class='collapse navbar-collapse justify-content-between' id='navbarNav'>
    <div class='row'>
    <div style='margin-right:2em; margin-left:2em;'>
      <a class='navbar-brand' href='index.php' style='font-size:30px'>Cabinet Médical</a>
    </div>
      <ul class='navbar-nav'>
        <li class='nav-item'>
          <a class='nav-link' href='patient.php' style='margin-top:10px'>Liste des patients</a>
        </li>
        <li class='nav-item'>
          <a class='nav-link' href='med.php' style='margin-top:10px'>Liste des médecins</a>
        </li>
        <li class='nav-item'>
          <a class='nav-link' href='consultations.php' style='margin-top:10px'>Consultations</a>
        </li>
        <li class='nav-item'>
          <a class='nav-link' href='stats.php' style='margin-top:10px'>Statistiques</a>
        </li>
      </ul>
      </div>
      <div class='row'>
        <div style='margin-right:2em; margin-top:10px;'>
          Connecté en tant que ".$_SESSION['id']."
        </div>
        <a class='btn btn-danger' href='logout.php'>Déconnexion</a>
      </div>
    </div>
  </nav>

</form>";
}
?>
