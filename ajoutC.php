<?php require 'connexionBD.php';
include 'outils.php';
IsConnectedSession();
connectedAndLogout();

$idP=$_POST['idP'];
$idM=$_POST['med'];
$date=$_POST['dateR'];
$heure=$_POST['heureD'];
$time=$_POST['temps'];

$datetime = $date." ".$heure;

//verif horaire pas deja pris
?>
<!DOCTYPE HTML>
<html>
  <head>
      <meta charset="utf-8" />
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
      <title>Léa passion PACES</title>
  </head>
  <body>
    <script src="bootstrap/js/jquery.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <?php
    $req = $linkpdo->prepare("SELECT COUNT(*) FROM `rdv` WHERE dateRDV=? AND idM=?");
    $res = $req->execute(array($datetime, $idM));
    if($res){
      if($req->fetchColumn() != 0){
        die("Le médecin est déja occupé à ce créneau-ci.");
      }else{
        echo "ajout en cours... <br>";
        $req = $linkpdo->prepare("INSERT INTO `rdv` VALUES(?, ?, ?, ?)");
        $res = $req->execute(array($idM, $idP, $datetime, $time));
        if($res == FALSE){
          echo "Erreur lors de l'ajout. <br>";
        }else{
          echo "rendez-vous enregistré. <br>";
        }
      }
    }else{
      die("erreur lors de la vérification.");
    }?>
</body>
</html>
